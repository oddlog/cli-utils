import os from "os";
import readline from "readline";
import {parse, SCHEMA_VERSION} from "@oddlog/record";

/*===================================================== Exports  =====================================================*/

export {SCHEMA_VERSION, pipe, trapSignals};

/*==================================================== Functions  ====================================================*/

/**
 * Pipes from input stream to output stream with options to modify behavior.
 *
 * The options may specify `input` and `output` for the streams to pipe from/to (default: stdin/stdout).
 * The options may specify `onLog`, `onEnd` and `onError` to attach a handler. `onLog` gets padded a Log instance and
 * the default log write function.
 * The options may specify `{Boolean|Function(line,printLn,err)} invalid=true` to alter the write of invalid lines.
 *
 * @param {Object} options The options.
 */
function pipe(options) {
  // extract options
  const streamIn = options.input || process.stdin;
  const streamOut = options.output || process.stdout;
  const logHandler = options.onLog || printLn;
  const endHandler = options.onEnd;
  const errHandler = options.onError || ((err) => {
    process.stderr.write(err.stack);
    process.exit(2);
  });
  const invalidHandler = typeof options.invalid === "function" ? options.invalid :
      options.invalid !== false ? (line, write) => write(line) : (() => {});

  // create reader
  const reader = readline.createInterface({input: streamIn});

  // attach handlers
  reader.on("line", (input) => {
    try { logHandler(parse(input), printLn); } catch (err) { invalidHandler(input, printLn, err); }
  });
  if (typeof errHandler === "function") { reader.on("error", errHandler); }
  if (typeof endHandler === "function") { reader.on("close", endHandler); }

  // write function to pass to logHandler
  function printLn(line) {
    streamOut.write(line);
    streamOut.write(os.EOL);
  }
}

/**
 * Attaches UNIX signal handlers as they are desired for most cli tools.
 *
 * Especially SIGINT should not be used for process exit. As this would cause logs being lost within bash pipes on such
 * a signal.
 */
function trapSignals() {
  process.on("SIGINT", () => {});
  process.on("SIGQUIT", () => process.exit(1));
  process.on("SIGTERM", () => process.exit(1));
}
